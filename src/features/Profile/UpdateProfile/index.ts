import UpdateProfile from '@/features/Profile/UpdateProfile/UpdateProfile.vue'
import UpdateAvatar from '@/features/Profile/UpdateProfile/UpdateAvatar.vue'

export { UpdateProfile, UpdateAvatar }
