import vueFilePond from 'vue-filepond'
import FilePondPluginFileValidateType from 'filepond-plugin-file-validate-type'
import FilePondPluginImagePreview from 'filepond-plugin-image-preview'
import FilePondPluginImageCrop from 'filepond-plugin-image-crop'
import { ref } from 'vue'
import type { FilePond } from 'filepond'

let instance: {
  FilePondComponent: ReturnType<typeof vueFilePond>
  pond: ReturnType<typeof ref<FilePond | null>>
} | null = null

export function useFilePond() {
  if (!instance) {
    const FilePondComponent = vueFilePond(
      FilePondPluginFileValidateType,
      FilePondPluginImagePreview,
      FilePondPluginImageCrop
    )
    const pond = ref<FilePond | null>(null)

    instance = {
      FilePondComponent,
      pond
    }
  }

  return instance
}
