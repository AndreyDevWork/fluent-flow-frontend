import { defineStore, type StoreDefinition } from 'pinia'
import { type ProfileResponse } from '@/shared/types/models/responses/profile'

export const useProfileStore: StoreDefinition<'profile', ProfileResponse> = defineStore('profile', {
  state: () => {
    return {
      about_me: null as string | null,
      date_of_birth: '',
      firstname: '',
      lastname: '',
      role: '',
      user: {
        created_at: new Date(),
        email_verified_at: null,
        id: 0,
        username: '',
        email: null
      },
      authorize: [],
      avatar: null
    }
  }
})
