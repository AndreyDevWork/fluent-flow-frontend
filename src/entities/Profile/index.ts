import TheProfile from '@/entities/Profile/TheProfile.vue'
import ProfileRightBoard from '@/entities/Profile/ui/ProfileRightBoard.vue'

export { TheProfile, ProfileRightBoard }
