import { computed } from 'vue'
import { AccessProfileScopesEnum } from '@/entities/Profile/Authorize/authorize.type.ts'
import { useProfileStore } from '@/entities/Profile/store/profile'

export function useCanUpdateProfile() {
  const profileStore = useProfileStore()

  const canUpdateProfile = computed(() => {
    return profileStore.authorize.some(
      (scope) => scope === AccessProfileScopesEnum.CAN_UPDATE_PROFILE
    )
  })

  return {
    canUpdateProfile
  }
}
