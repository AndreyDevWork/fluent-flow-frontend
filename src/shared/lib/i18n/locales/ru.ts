import { ruValidate } from '@/shared/lib/i18n/locales/validate/ruValidate.ts'
import { ruBanners } from '@/shared/lib/i18n/locales/banners/ruBanners.ts'
import { ruCards } from '@/shared/lib/i18n/locales/cards/ruCards.ts'

const appName = import.meta.env.VITE_APP_NAME

export const ru = {
  app: {
    name: appName,
    validate: ruValidate
  },
  features: {
    cards: ruCards
  },
  banners: ruBanners,
  cancel: 'Отмена',
  dateOfBirth: 'День рождения',
  aboutMe: 'Обо мне',
  firstname: 'Имя',
  lastname: 'Фамилия',
  username: 'Имя пользователя',
  email: 'Email',
  password: 'Пароль',
  signUp: 'Зарегестрироваться',
  signIn: 'Войти',
  enterInAccount: 'Вход',
  toSignIn: 'Перейти на страницу входа',
  create: 'Создать',
  languages: 'Языки',
  createAccountOnFluentFlow: `Создайте аккаунт на ${appName}`,
  firstTimeOnHereCreateAccount: `Впервые на ${appName}? Создайте аккаунт`,
  alreadyRegistered: 'Уже есть учетная запись? Войти',
  creatingProfile: 'Создание профиля',
  createProfile: 'Создать профиль',
  dragYourAvatarHereOr: 'Перетащите ваш аватар сюда или',
  selectAfile: 'Выберите файл',
  uploadYourAvatar: 'Загрузите аватар',
  account: 'Аккаунт',
  pleaseVerifyEmail: 'Пожалуйста, верифицируйте ваш email.',
  checkYourMailForConfirmation: 'Проверьте вашу почту для подтверждения.',
  sendLetter: 'Отправить письмо',
  resend: 'Отправить повторно'
}
