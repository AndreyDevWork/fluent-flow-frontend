import { frValidate } from '@/shared/lib/i18n/locales/validate/frValidate.ts'
import { frBanners } from '@/shared/lib/i18n/locales/banners/frBanners.ts'
import { frCards } from '@/shared/lib/i18n/locales/cards/frCards.ts'

const appName = import.meta.env.VITE_APP_NAME

export const fr = {
  app: {
    name: 'Fluent Flow',
    validate: frValidate
  },
  features: {
    cards: frCards
  },
  banners: frBanners,
  cancel: 'Annuler',
  dateOfBirth: 'Date de naissance',
  firstname: 'Prénom',
  aboutMe: 'Sur moi',
  lastname: 'Nom de famille',
  username: "Nom d'utilisateur",
  email: 'Email',
  password: 'Mot de passe',
  signUp: "S'inscrire",
  signIn: 'Se connecter',
  enterInAccount: 'Se connecter',
  toSignIn: 'Aller à la page de connexion',
  create: 'Créer',
  languages: 'Langages',
  createAccountOnFluentFlow: `Créer un compte sur ${appName}`,
  firstTimeOnHereCreateAccount: `Vous débutez avec ${appName}? Créer un compte`,
  alreadyRegistered: 'Vous avez déjà un compte? Se connecter',
  creatingProfile: 'Créer un profil',
  createProfile: 'Créer un profil',
  dragYourAvatarHereOr: 'Faites glisser votre avatar ici ou',
  selectAfile: 'Sélectionnez un fichier',
  uploadYourAvatar: 'Téléchargez votre avatar',
  account: 'Compte',
  pleaseVerifyEmail: 'Veuillez vérifier votre email.',
  checkYourMailForConfirmation: 'Vérifiez votre courrier électronique pour confirmer.',
  sendLetter: 'Envoyer une lettre',
  resend: 'Renvoyer'
}
