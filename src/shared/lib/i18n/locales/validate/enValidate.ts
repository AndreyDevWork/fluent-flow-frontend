export const enValidate = {
  required: 'Required',
  minLength: 'Please use at least {length} characters',
  wrongFormat: 'Wrong format'
}
