export const frValidate = {
  required: 'Obligatoire',
  minLength: 'Veuillez utiliser au minimum {length} caractères',
  wrongFormat: 'Mauvais format'
}
