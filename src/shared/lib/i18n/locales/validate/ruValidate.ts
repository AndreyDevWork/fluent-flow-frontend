export const ruValidate = {
  required: 'Обязательное поле',
  minLength: 'Минимум {length} символов',
  wrongFormat: 'Неверный формат'
}
