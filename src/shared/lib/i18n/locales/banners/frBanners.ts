export const frBanners = {
  serverErrors: {
    signUpWithUsername: "Utilisateur avec ce nom d'utilisateur existe déjà",
    wrongLoginOrPassword: 'Identifiant ou mot de passe incorrect'
  },
  pageNotFound: 'PAGE INTROUVABLE - 404',
  profileNotFound: 'PROFIL INTROUVABLE',
  toProfilePage: 'Mon profil',
  successSignUp: 'Félicitations! Vous avez créé un compte!'
}
