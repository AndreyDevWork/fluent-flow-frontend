export const enBanners = {
  serverErrors: {
    signUpWithUsername: 'User with this username already exists',
    wrongLoginOrPassword: 'Wrong login or password'
  },
  pageNotFound: 'PAGE NOT FOUND - 404',
  profileNotFound: 'PROFILE NOT FOUND',
  toProfilePage: 'My Profile',
  successSignUp: 'Congratulations! You have created an account!'
}
