export const ruBanners = {
  serverErrors: {
    signUpWithUsername: 'Пользователь с таким именем пользователя уже существует',
    wrongLoginOrPassword: 'Неверный логин или пароль'
  },
  pageNotFound: 'СТРАНИЦА НЕ НАЙДЕНА - 404',
  profileNotFound: 'ПРОФИЛЬ НЕ НАЙДЕН',
  toProfilePage: 'Мой профиль',
  successSignUp: 'Поздравляем! Вы создали аккаунт'
}
