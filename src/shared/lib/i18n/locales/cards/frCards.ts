export const frCards = {
  cards: 'Cartes',
  sets: 'Ensembles',
  createCard: 'Créer une carte',
  term: 'Terme',
  translate: 'Traduire'
}
