import { enValidate } from '@/shared/lib/i18n/locales/validate/enValidate.ts'
import { enBanners } from '@/shared/lib/i18n/locales/banners/enBanners.ts'
import { enCards } from '@/shared/lib/i18n/locales/cards/enCards.ts'

const appName = import.meta.env.VITE_APP_NAME

export const en = {
  app: {
    name: import.meta.env.VITE_APP_NAME,
    validate: enValidate
  },
  features: {
    cards: enCards
  },
  banners: enBanners,
  cancel: 'Cancel',
  dateOfBirth: 'Date of birth',
  firstname: 'Firstname',
  aboutMe: 'About Me',
  lastname: 'Lastname',
  username: 'Username',
  email: 'Email',
  password: 'Password',
  signUp: 'Sign up',
  signIn: 'Sign in',
  enterInAccount: 'Sign in',
  toSignIn: 'Go to login page',
  create: 'Create',
  languages: 'languages',
  createAccountOnFluentFlow: `Create an account on ${appName}`,
  firstTimeOnHereCreateAccount: `New to ${appName}? Create an account`,
  alreadyRegistered: 'Already have an account? Sign in',
  creatingProfile: 'Creating a Profile',
  createProfile: 'Create profile',
  dragYourAvatarHereOr: 'Drag your avatar here or',
  selectAfile: 'Select a file',
  uploadYourAvatar: 'Upload your avatar',
  account: 'Account',
  pleaseVerifyEmail: 'Please verify your email.',
  checkYourMailForConfirmation: 'Check your mail for confirmation',
  sendLetter: 'Send a letter',
  resend: 'Resend'
}
