import api from '@/shared/lib/axios/axiosInstance.ts'
import type { AxiosResponse } from 'axios'
import type { SignInResponse, SignUpResponse } from '@/shared/types/models/responses/auth'
import type { SignInRequest, SignUpRequest } from '@/shared/types/models/requests/auth'
import { LocalStorage } from 'quasar'
import { jwtDecode, type JwtPayload } from 'jwt-decode'

export default class AuthService {
  static async signIn(credentials: SignInRequest): Promise<AxiosResponse<SignInResponse>> {
    return api.post<SignInResponse>('/oauth/token', credentials)
  }

  static async signUp(credentials: SignUpRequest): Promise<AxiosResponse<SignUpResponse>> {
    return api.post<SignUpResponse>('/api/auth/register', credentials)
  }

  static async verifyEmail(): Promise<AxiosResponse> {
    return api.post<AxiosResponse>('/api/auth/send-email-verification-message')
  }

  static logout(): void {
    LocalStorage.remove('ACCESS_TOKEN')
    localStorage.removeItem('REFRESH_TOKEN')
  }

  static isAuth(): boolean {
    return !!LocalStorage.getItem<boolean>('ACCESS_TOKEN')
  }

  static getAccessToken(): string {
    return LocalStorage.getItem<string>('ACCESS_TOKEN') || ''
  }

  static getAccessTokenDecode(): JwtPayload {
    return jwtDecode(this.getAccessToken())
  }

  static async refresh(): Promise<void> {
    try {
      const request = await AuthService.signIn({
        client_secret: import.meta.env.VITE_CLIENT_SECRET,
        client_id: import.meta.env.VITE_CLIENT_ID,
        grant_type: 'refresh_token',
        refresh_token: LocalStorage.getItem('REFRESH_TOKEN') as string
      })

      localStorage.setItem('ACCESS_TOKEN', request.data.access_token)
      localStorage.setItem('REFRESH_TOKEN', request.data.refresh_token)
    } catch {
      this.logout()
    }
  }

  static async refreshIfTokenIsDead() {
    if (AuthService.isAuth()) {
      const expirationTime = new Date(Number(AuthService.getAccessTokenDecode().exp) * 1000)
      const currentTime = new Date()

      if (currentTime > expirationTime) {
        await AuthService.refresh()
      }
    }
  }
}
