import AuthService from '@/shared/api/services/AuthService/AuthService.ts'
import ProfileService from '@/shared/api/services/ProfileService/ProfileService.ts'

export { AuthService, ProfileService }
