import api from '@/shared/lib/axios/axiosInstance.ts'
import type { AxiosResponse } from 'axios'
import type {
  Avatar,
  ProfileResponse
} from '@/shared/types/models/responses/profile/ProfileResponse.ts'
import type { StoreRequest } from '@/shared/types/models/requests/profile/StoreRequest.ts'
import type { FilePondFile } from 'filepond'

export default class ProfileService {
  static async show(userId: number): Promise<AxiosResponse<ProfileResponse>> {
    return await api.get<ProfileResponse>(`/api/profile/${userId}`)
  }

  static async store(params: {
    data: StoreRequest
    images: FilePondFile[]
  }): Promise<AxiosResponse<ProfileResponse>> {
    const formData = new FormData()

    Object.entries(params.data).forEach(([key, value]) => {
      formData.append(key, String(value))
    })
    params.images.forEach((file) => {
      formData.append('images[]', file.file)
    })

    return await api.post<ProfileResponse>('/api/profile', formData)
  }

  static async uploadAvatar(params: { images: FilePondFile[] }): Promise<AxiosResponse<Avatar>> {
    const formData = new FormData()
    params.images.forEach((file) => {
      formData.append('images[]', file.file)
    })

    return await api.post<Avatar>('/api/profile/avatar', formData)
  }

  static async update(params: {
    data: StoreRequest
    userId: number
  }): Promise<AxiosResponse<ProfileResponse>> {
    return await api.patch<ProfileResponse>(`api/profile/${params.userId}`, params.data)
  }
}
