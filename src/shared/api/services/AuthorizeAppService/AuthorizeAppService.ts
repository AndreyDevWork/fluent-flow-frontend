import { ref } from 'vue'
import type { AccessAppScopesEnum } from '@/shared/types/enums/authorize.types.ts'
import api from '@/shared/lib/axios/axiosInstance.ts'
import { AuthService } from '@/shared/api/services'

function createService() {
  const userScopes = ref<Record<AccessAppScopesEnum, boolean | undefined> | null>(null)

  async function fetchUserOnes(): Promise<void> {
    await AuthService.refreshIfTokenIsDead()

    const req = await api.get('/api/get-authorize-app')
    userScopes.value = req.data.reduce(
      (acc: Record<AccessAppScopesEnum, boolean | undefined>, curr: AccessAppScopesEnum) => {
        acc[curr] = true
        return acc
      },
      {}
    )
  }

  function checkHasScope(scopes: AccessAppScopesEnum[]) {
    if (!userScopes.value) return false

    return scopes.some((scope) => (userScopes.value ? userScopes.value[scope] : false))
  }

  return {
    fetchUserOnes,
    checkHasScope,
    userScopes
  }
}

type IService = ReturnType<typeof createService>
let singletonService: ReturnType<typeof createService> | null = null

function useService(): IService {
  if (!singletonService) singletonService = createService()
  return singletonService
}

export { createService as createAuthService, useService as useAuthorizeService }
