import api from '@/shared/lib/axios/axiosInstance.ts'
import type { AxiosResponse } from 'axios'
import type { CardRequest } from '@/shared/types/models/requests/card/CardRequest.ts'
import type {
  CardListResponse,
  CardResponse
} from '@/shared/types/models/responses/card/CardResponse.ts'

export default class CardService {
  static async store(params: { data: CardRequest }): Promise<AxiosResponse<CardResponse>> {
    return await api.post<CardResponse>('api/card', params.data)
  }

  static async index(): Promise<AxiosResponse<CardListResponse>> {
    return await api.get<CardListResponse>('api/card')
  }
}
