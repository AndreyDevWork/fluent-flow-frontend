import { i18n } from '@/shared/lib/i18n'

const requiredValidator = (val: any) => !!val || i18n.global.t('app.validate.required')
const passwordLengthValidator = (val: string) =>
  val.length >= 8 || i18n.global.t('app.validate.minLength', { length: 8 })

const firstnameLengthValidator = (val: string) =>
  val.length >= 2 || i18n.global.t('app.validate.minLength', { length: 2 })

const aboutMeLengthValidator = (val: string) =>
  val.length >= 11 || i18n.global.t('app.validate.minLength', { length: 11 })

const dateFormatValidator = (val: any) => {
  const dateFormatRegex = /^\d{4}[-/]\d{2}[-/]\d{2}$/
  return dateFormatRegex.test(val) || i18n.global.t('app.validate.wrongFormat')
}

export const validate = {
  required: [requiredValidator],
  password: [requiredValidator, passwordLengthValidator],
  firstname: [requiredValidator, firstnameLengthValidator],
  lastname: [requiredValidator, firstnameLengthValidator],
  aboutMe: [requiredValidator, aboutMeLengthValidator],
  date: [requiredValidator, dateFormatValidator]
}
