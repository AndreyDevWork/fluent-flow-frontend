import type { AccessProfileScopesEnum } from '@/entities/Profile/Authorize/authorize.type.ts'

interface User {
  created_at: Date
  email_verified_at: Date | null
  id: number
  username: string
  email: string | null
}

interface Avatar {
  url: string
  updated_at: Date
  created_at: Date
}

interface ProfileResponse {
  about_me: string | null
  date_of_birth: string
  firstname: string
  lastname: string
  role: string
  user: User
  authorize: AccessProfileScopesEnum[]
  avatar: Avatar | null
}

export type { ProfileResponse, User, Avatar }
