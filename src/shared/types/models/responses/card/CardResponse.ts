import type { User } from '@/shared/types/models/responses/profile/ProfileResponse.ts'

interface StatusCard {
  id: number
  status: string
}

interface Author {
  firstname: string
  lastname: string
  user: User
}

interface CardResponse {
  id: number
  term: string
  translate: string
  status: StatusCard
  author: Author
  authorize: []
}

interface CardListResponse {
  data: CardResponse[]
}

export type { CardResponse, CardListResponse }
