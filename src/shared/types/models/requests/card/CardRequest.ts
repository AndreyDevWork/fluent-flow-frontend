export interface CardRequest {
  term: string
  translate: string
  status_id: number
}
