export interface StoreRequest {
  firstname: string
  lastname: string
  email?: string
  date_of_birth: string
  about_me: string | null
  role_id?: number
}
