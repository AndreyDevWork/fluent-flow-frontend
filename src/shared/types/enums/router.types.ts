import type { VueElement } from 'vue'
import type { AccessAppScopesEnum } from '@/shared/types/enums/authorize.types.ts'

declare module 'vue-router' {
  interface RouteMeta {
    layoutComponent?: VueElement
    accessScopes?: AccessAppScopesEnum[]
  }
}

export enum RouteNamesEnum {
  signUp = 'signUp',
  signIn = 'signIn',
  profile = 'profile',
  home = 'home',
  cards = 'cards',
  sets = 'sets',
  profileStore = 'profileStore',
  accessError = 'accessError',
  pageNotFound = 'pageNotFound'
}
