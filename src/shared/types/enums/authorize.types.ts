export enum AccessAppScopesEnum {
  CAN_USE_APP = 'CAN_USE_APP',
  IS_AUTHENTICATED = 'IS_AUTHENTICATED',
  IS_NOT_AUTHENTICATED = 'IS_NOT_AUTHENTICATED',
  CAN_USE_PROFILE = 'CAN_USE_PROFILE',
  CAN_CREATE_PROFILE = 'CAN_CREATE_PROFILE'
}
