import ServerErrorBanner from '@/shared/ui/banners/ServerErrorBanner.vue'
import NotFoundBanner from '@/shared/ui/banners/NotFoundBanner.vue'

export { ServerErrorBanner, NotFoundBanner }
