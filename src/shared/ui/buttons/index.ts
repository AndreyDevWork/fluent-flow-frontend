import LoadingButton from '@/shared/ui/buttons/LoadingButton.vue'
import EditButton from '@/shared/ui/buttons/EditButton.vue'
import CancelButton from '@/shared/ui/buttons/CancelButton.vue'
import SaveButton from '@/shared/ui/buttons/SaveButton.vue'

export { LoadingButton, EditButton, CancelButton, SaveButton }
