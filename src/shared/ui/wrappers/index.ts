import AttributeWithIcon from '@/shared/ui/wrappers/AttributeWithIcon.vue'
import AttributeWrapper from '@/shared/ui/wrappers/AttributeWrapper.vue'
import BoardWrapper from '@/shared/ui/wrappers/BoardWrapper.vue'

export { AttributeWithIcon, AttributeWrapper, BoardWrapper }
