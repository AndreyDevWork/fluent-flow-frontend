import FirstnameInput from '@/shared/ui/inputs/FirstnameInput.vue'
import LastnameInput from '@/shared/ui/inputs/LastnameInput.vue'
import AboutMeInput from '@/shared/ui/inputs/AboutMeInput.vue'
import PasswordInput from '@/shared/ui/inputs/PasswordInput.vue'
import UsernameInput from '@/shared/ui/inputs/UsernameInput.vue'
import AppInput from '@/shared/ui/inputs/AppInput.vue'

export { FirstnameInput, LastnameInput, AboutMeInput, PasswordInput, UsernameInput, AppInput }
