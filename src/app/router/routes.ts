import { type RouterOptions } from 'vue-router'
import { RouteNamesEnum } from '@/shared/types/enums/router.types.ts'
import { AccessAppScopesEnum } from '@/shared/types/enums/authorize.types.ts'

export const routes: RouterOptions['routes'] = [
  {
    path: '/sign-up',
    name: RouteNamesEnum.signUp,
    component: () => import('@/pages/Auth/SignUpPage'),
    meta: {
      accessScopes: [AccessAppScopesEnum.IS_NOT_AUTHENTICATED],
      requiresAuth: false
    }
  },
  {
    path: '/sign-in',
    name: RouteNamesEnum.signIn,
    component: () => import('@/pages/Auth/SignInPage'),
    meta: {
      accessScopes: [AccessAppScopesEnum.IS_NOT_AUTHENTICATED],
      requiresAuth: false
    }
  },
  {
    path: '/profile/:id',
    name: RouteNamesEnum.profile,
    component: () => import('@/pages/Profile/ProfilePage'),
    meta: {
      accessScopes: [AccessAppScopesEnum.CAN_USE_PROFILE],
      requiresAuth: true
    }
  },
  {
    path: '/profile/store',
    name: RouteNamesEnum.profileStore,
    component: () => import('@/pages/Profile/ProfileStorePage'),
    meta: {
      accessScopes: [AccessAppScopesEnum.CAN_CREATE_PROFILE],
      requiresAuth: true
    }
  },
  {
    path: '/access-error',
    name: RouteNamesEnum.accessError,
    component: () => import('@/pages/Errors/AccessErrorPage'),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/',
    name: RouteNamesEnum.home,
    component: () => import('@/pages/Home'),
    meta: {
      requiresAuth: true,
      accessScopes: [AccessAppScopesEnum.CAN_USE_PROFILE]
    }
  },
  {
    path: '/cards',
    name: RouteNamesEnum.cards,
    component: () => import('@/pages/Cards/CardPage')
  },
  {
    path: '/sets',
    name: RouteNamesEnum.sets,
    component: () => import('@/pages/Cards/SetPage')
  },
  {
    path: '/:pathMatch(.*)',
    name: RouteNamesEnum.pageNotFound,
    component: () => import('@/pages/Errors/PageNotFound')
  }
]
