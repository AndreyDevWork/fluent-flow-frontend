import { useAuthorizeService } from '@/shared/api/services/AuthorizeAppService/AuthorizeAppService.ts'
import type { RouteLocationNormalized } from 'vue-router'
import { RouteNamesEnum } from '@/shared/types/enums/router.types.ts'

export async function createProfileMiddleware(to: RouteLocationNormalized) {
  const { userScopes } = useAuthorizeService()
  if (
    to.name !== RouteNamesEnum.profileStore &&
    userScopes.value?.CAN_CREATE_PROFILE &&
    userScopes.value?.IS_AUTHENTICATED
  ) {
    return {
      name: RouteNamesEnum.profileStore
    }
  }

  return
}
