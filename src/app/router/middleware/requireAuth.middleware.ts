import type { RouteLocationNormalized } from 'vue-router'
import AuthService from '../../../shared/api/services/AuthService/AuthService.ts'
import { useAuthorizeService } from '@/shared/api/services/AuthorizeAppService/AuthorizeAppService.ts'
import { RouteNamesEnum } from '@/shared/types/enums/router.types.ts'

export function requireAuthMiddleware(to: RouteLocationNormalized) {
  const { userScopes } = useAuthorizeService()

  if (to.meta.requiresAuth && userScopes.value?.IS_NOT_AUTHENTICATED) {
    return {
      name: RouteNamesEnum.signIn
    }
  } else if (
    (to.name === RouteNamesEnum.signIn || to.name === RouteNamesEnum.signUp) &&
    userScopes.value?.IS_AUTHENTICATED
  ) {
    return {
      name: RouteNamesEnum.profile,
      params: { id: AuthService.getAccessTokenDecode().sub }
    }
  }
  return
}
