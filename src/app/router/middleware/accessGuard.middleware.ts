import type { RouteLocationNormalized } from 'vue-router'
import { useAuthorizeService } from '@/shared/api/services/AuthorizeAppService/AuthorizeAppService.ts'
import { RouteNamesEnum } from '@/shared/types/enums/router.types.ts'

export function accessGuardMiddleware(to: RouteLocationNormalized) {
  const { accessScopes } = to.meta
  if (!accessScopes) return

  const { checkHasScope } = useAuthorizeService()
  if (checkHasScope(accessScopes)) return

  return { name: RouteNamesEnum.accessError }
}
