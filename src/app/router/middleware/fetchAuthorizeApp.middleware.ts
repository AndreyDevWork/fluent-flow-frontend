import { useAuthorizeService } from '@/shared/api/services/AuthorizeAppService/AuthorizeAppService.ts'

export function fetchAuthorizeAppMiddleware(): Promise<void> {
  const { fetchUserOnes } = useAuthorizeService()
  return fetchUserOnes()
}
