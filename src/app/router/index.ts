import { createRouter, createWebHistory, type Router } from 'vue-router'
import { routes } from './routes'
import { fetchAuthorizeAppMiddleware } from '@/app/router/middleware/fetchAuthorizeApp.middleware.ts'
import { accessGuardMiddleware } from '@/app/router/middleware/accessGuard.middleware.ts'
import { requireAuthMiddleware } from '@/app/router/middleware/requireAuth.middleware.ts'
import { createProfileMiddleware } from '@/app/router/middleware/createProfile.middleware.ts'
import type { AccessAppScopesEnum } from '@/shared/types/enums/authorize.types.ts'

declare module 'vue-router' {
  interface RouteMeta {
    accessScopes?: AccessAppScopesEnum[]
    requiresAuth: boolean
  }
}

const router: Router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes
})

router.beforeEach(fetchAuthorizeAppMiddleware)
router.beforeEach(createProfileMiddleware)
router.beforeEach(requireAuthMiddleware)
router.beforeEach(accessGuardMiddleware)

export default router
