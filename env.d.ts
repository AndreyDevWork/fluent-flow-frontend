/// <reference types="vite/client" />
interface ImportMetaEnv {
  VITE_API_URL: string
  VITE_CLIENT_ID: string
  VITE_CLIENT_SECRET: string
  VITE_APP_NAME: string
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}
